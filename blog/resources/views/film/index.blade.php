@extends('layout.master')

@section('judul')
Halaman List Film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>    
@endauth 
    @forelse ($film as $item)
            
            <form action="/film/{{$item->id}}" method="POST">
                    @auth
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    @endauth
                  </form>
              </td>
          </tr>
      @empty
          <h1>Data tidak ada</h1>
      @endforelse
        

@endsection