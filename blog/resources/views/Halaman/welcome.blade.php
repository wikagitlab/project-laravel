@extends('layout.master')

@section('judul')
Welcome
@endsection

@section('content')

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>SELAMAT DATANG! {{$firstname}} {{$lastname}}</h1>
    <H3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</H3>
@endsection