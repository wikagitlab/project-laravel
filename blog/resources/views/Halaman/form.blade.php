@extends('layout.master')

@section('judul')
Halaman Register
@endsection

@section('content')

    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome"method="post">
        @csrf
       <label>First Name:</label><br><br>
       <input type="text" name="firstname"><br><br>
       <label>Last Name:</label><br><br>
       <input type="text" name="lastname"><br><br>
       <label>Gender</label><br><br>
       <input type="radio">Male <br>
       <input type="radio">Female <br><br>
       <label>Nationality</label><br><br>
       <select name="Nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Filipina">Filipina</option>
        <option value="Filipina">Filipina</option>
       </select><br><br>
       <label>Language Spoken</label><br><br>
       <input type="checkbox" name="Language Spoken">Bahasa Indonesia <br>
       <input type="checkbox" name="Language Spoken">English <br>
       <input type="checkbox" name="Language Spoken">Other <br><br>
       <label>Bio</label><br><br>
       <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>
       <input type="Submit" value="Sign Up">

    </form>
@endsection