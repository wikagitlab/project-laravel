@extends('layout.master')

@section('judul')
Halaman Update Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Nama User</label>
    <input type="text" value= "{{$profile->user->name}}" class="form-control" disabled>
  </div>
  <div class="form-group">
    <label>Email User</label>
    <input type="text" value= "{{$profile->user->email}}" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Umur Profile</label>
    <input type="number" name="umur" value= "{{$profile->umur}}" class="form-control" >
  </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <label>Alamat</label>
    <textarea name="alamat" value= "{{$profile->alamat}}" class="form-control"></textarea>
  </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection