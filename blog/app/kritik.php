<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table = 'kritik';

    protected $fillable = ['user_id', 'film_id', 'content', 'point'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function film()
    {
        return $this->belongsTo('App\film');
    }
}
