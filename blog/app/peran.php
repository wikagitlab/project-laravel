<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peran extends Model
{
    protected $table = 'kritik';

    protected $fillable = ['film_id', 'cast_id', 'nama'];

    public function cast()
    {
        return $this->belongsTo('App\cast');
    }

    public function film()
    {
        return $this->belongsTo('App\film');
    }

}
