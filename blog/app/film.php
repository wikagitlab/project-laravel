<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = 'film';

    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    public function genre()
    {
        return $this->belongsTo('App\genre');
    }

    public function kritik()
    {
        return $this->hasMany('App\kritik');
    }
    
    public function peran()
    {
        return $this->hasMany('App\peran');
    }
}
